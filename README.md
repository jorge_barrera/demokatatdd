# README #

Realizar una calculadora de strings
- La calculadora puede procesar un string en el que vienen 0,1 o 2 números separados por coma.
- Si vienen dos números devuelve la suma de los dos
- Si viene un número devuelve el numero
- Si el string viene vacio devuelve 0

### Autor ###

* Enlace del repositorio: https://jorge_barrera@bitbucket.org/jorge_barrera/demokatatdd.git
* Nombre Completo: Jorge Enrique Barrera Salgado
* Código Uniandes: 201510887
* Cuenta uniandes: je.barrera11@uniandes.edu.co