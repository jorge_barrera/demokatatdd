from unittest import TestCase

from calculadora import calculadora


class calculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(calculadora().sumar(""), 0, "Cadena Vacia")

    def test_sumar_cadena(self):
        self.assertEqual(calculadora().sumar("1"), 1, "Un Numero")

    def test_sumar_numero(self):
        self.assertEqual(calculadora().sumar("1"), 1, "Un Numero")
        self.assertEqual(calculadora().sumar("2"), 2, "Un Numero")

    def test_sumar_cadena_numero(self):
        self.assertEqual(calculadora().sumar("1,3"), 4, "Dos Numeros")

    def test_sumar_cadena_multiple_numero(self):
        self.assertEqual(calculadora().sumar("5,2,4,1"), 12, "Multiples Numeros")

    def test_sumar_cadena_multiple_numero_separador(self):
        self.assertEqual(calculadora().sumar("5,2&4:1:2&8"), 22, "Multiples Numeros Distintos Separadores")
